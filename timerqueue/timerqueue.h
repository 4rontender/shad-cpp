#pragma once

#include <mutex>
#include <condition_variable>
#include <chrono>
#include <map>

template <class T>
class TimerQueue {
public:
    void Add(const T& item, std::chrono::system_clock::time_point at);

    T Pop();
};
